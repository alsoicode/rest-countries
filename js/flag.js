'use strict';

const regionMenu = document.getElementById('regionMenu');
const result = document.getElementById('result');
const countryUrl = 'https://restcountries.eu/rest/v2/all';
let countries;

/**
 *
 * @param countryUrl
 * string for the URL of the web service providing "country" objects
 *
 * You proobably want to cache this response.
*/
fetch(countryUrl)
  .then(response => response.json())
  .then(json => {
    countries = json;
    createOutput(countries);
  })
  .catch(error => {
    console.log(error)
  });

/**
 *
 * @param countryObjects
 * An array of "country" objects
 *
 * Always name functions closer to what they actually do.
 * It cuts down on cognitive overhead keeping track of shortcuts &/or acronyms
 */
function createOutput(countryObjects) {
    let output = '';

    // Since you have to iterate over the entire list, you can simply use forEach
    countryObjects.forEach(country => {
      output += `
        <div>
          <img src="${ country.flag }" width="160px" height="80px">
          <ul>
            <li>Name: ${ country.name }</li>
            <li>Population: ${ country.population }</li>
            <li>Region: ${ country.region }</li>
            <li>Capital: ${ country.capital }</li>
          </ul>
        </div>
        `
    });

    result.innerHTML = output;
}

function filterCountries() {
  // get the currently selected value
  const selectedRegion = this.value;
  let countryObjects;

  // if the selected value is 'all', use the existing list of countries assigned during the
  // initial web service call
  if (selectedRegion === 'all') {
    countryObjects = countries;
  }
  else {
    // otherwise, filter the array of countries where the region matches
    // the currently selected region
    countryObjects = countries.filter(country => country.region === selectedRegion);
  }

  createOutput(countryObjects);
}

regionMenu.addEventListener('change', filterCountries);
